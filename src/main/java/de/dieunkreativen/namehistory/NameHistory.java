package de.dieunkreativen.namehistory;

import java.util.logging.Logger;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class NameHistory extends JavaPlugin{
	Logger log;
	PluginDescriptionFile pdfFile = this.getDescription();

    public void onLoad() {
    	log = getLogger();
    }
    
	@Override
	public void onEnable() {
		this.getCommand("uuid").setExecutor(new CmdExecutor(this));
		this.getCommand("names").setExecutor(new CmdExecutor(this));
        log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!");
	}
	
	@Override
	public void onDisable() {
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is disabled!");
	}
}
