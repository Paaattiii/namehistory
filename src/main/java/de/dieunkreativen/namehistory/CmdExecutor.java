package de.dieunkreativen.namehistory;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CmdExecutor implements CommandExecutor{
	NameHistory plugin;
	
	public CmdExecutor(NameHistory instance) {
		plugin = instance;
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("names")) {
			if(args.length == 1) {
				UUIDFetcher.getUUID(args[0], new Consumer<UUID>() {
		              @Override
		              public void accept(UUID t) {
		            	  if (t == null) {
		            		  sender.sendMessage(ChatColor.RED + "Dieser Name existiert nicht!");
		            		  return;
		            	  }
		            	  Date lastChange = UUIDFetcher.getNameHistory(t).firstEntry().getKey();
		            	  Calendar cal = Calendar.getInstance();
		                  cal.setTime(lastChange);
		                  cal.add(Calendar.DATE, 30);
		                  SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		                  String formatted = format1.format(cal.getTime());
		            	  sender.sendMessage("------ " + ChatColor.GOLD + "Verlauf von " + args[0] + ChatColor.WHITE + " ------");
		            	  
		            	  for(Map.Entry<Date, String> e : UUIDFetcher.getNameHistory(t).entrySet()){
		            		  	Date date = e.getKey();
		            		  String name = e.getValue();
		            		  if (date.getTime() < 1422748800000L) {
		            			  sender.sendMessage(ChatColor.DARK_AQUA + "[Original]" + " " + ChatColor.GOLD + name);
		            		  }
		            		  else {
		            			  sender.sendMessage(ChatColor.DARK_AQUA + "[" + date.toString() + "]" + " " + ChatColor.GOLD + name);
		            		  }
		            	  }
		            	  Calendar calendar = Calendar.getInstance();
		            	  if (cal.getTime().getTime() < calendar.getTime().getTime()) {
		            		  sender.sendMessage(ChatColor.GREEN + "N�chste Namens�nderung m�glich ab: " + " " + ChatColor.DARK_GREEN +
			            			  "sofort verf�gbar");  
		            	  }
		            	  else {
		            		  sender.sendMessage(ChatColor.GREEN + "N�chste Namens�nderung m�glich ab: " + " " + ChatColor.RED +
		            			  formatted);
		            	  }
		              }
		            });
				return true;
			}
			sender.sendMessage(ChatColor.RED + "/names <Name>");
		}
		if (cmd.getName().equalsIgnoreCase("uuid")) {
			if(args.length == 1) {
				UUIDFetcher.getUUID(args[0], new Consumer<UUID>() {
		              @Override
		              public void accept(UUID t) {
		            	  sender.sendMessage(t.toString());
		              }
		            });
				return true;
			}
		}
		return false;
	}

}

